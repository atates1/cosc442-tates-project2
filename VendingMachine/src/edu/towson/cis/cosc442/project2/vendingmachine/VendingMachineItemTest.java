package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineItemTest {

	private VendingMachineItem v1,v2, v3;
	@Before
	public void setUp() throws Exception {
		v1 = new VendingMachineItem("snickers",1.50);
		v2 = new VendingMachineItem("Twizzlers",2.00);
		
	}

	

	@Test /* test that the values are being passed through the constructor */
	public void constructorTest() {
		assertThat("snickers", is(v1.getName()));
		assertThat(1.50, is(v1.getPrice()));
		
		//fail("Not yet implemented");
	}
	
	
		
		
	@Test /* test the name method */
	public void getNameTest() {
		assertEquals("Twizzlers", v2.getName());
	}
	
	@SuppressWarnings("deprecation")
	@Test /* test the price method */
	public void getPriceTest() {
		assertEquals(2.00, v2.getPrice(),0.01);
		
	}
	@SuppressWarnings("deprecation")
	@Test(expected = NullPointerException.class)
	public void ExceptionTest() {
		assertEquals(-2.00, v3.getPrice(),0.01);
		
		v3 = new VendingMachineItem("air",-2.00);
	}
	
	
	
	
	@After
	public void tearDown() throws Exception {
		v1 = null;
		v2 = null;
	}
}
