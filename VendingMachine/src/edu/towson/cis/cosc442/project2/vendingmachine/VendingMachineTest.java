package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest {

	private VendingMachineItem candy,choc,gum;
	private VendingMachine v1,v2,v3;
	
	
	
	
	@Before
	public void setUp() throws Exception {
		candy = new VendingMachineItem("snicker", 99999);
		choc = new VendingMachineItem("hershey", 99999);
		gum = new VendingMachineItem("gum",99999);
		
	}
	
	@Test 
	/*test to see if an item was added to the vending machine*/
	public void addItemTest() {
		
		v1.addItem(candy,"snicker");
		assertEquals(v1.getItem("snicker").getName(), "snicker");
		
	}
	
	@Test(expected=NullPointerException.class)
	/*test to make sure that if an item code is wrong its not added*/
	public void addItemTestFail() {
		v2.addItem(choc,"B");
		
		assertNull(v2.getItem("Z"));
	}
	
	@Test
	public void getBalanceTest() {
		assertEquals(v1.getBalance(), 0.0,0.0);
	}
	@Test 
	public void removeTest() {
		v1.addItem(candy,"A");
		v1.removeItem("A");
		
		assertNull(v1.getItem("A"));
	}
	
	@Test
	public void insertMoneyTest() {
		v1.insertMoney(2.00);
	}
	
	
	@Test
	public void makePurchase() {
		v1.addItem(candy,"A");
		v1.insertMoney(2.00);
		
		assertEquals(v1.makePurchase("A"), true);
	}
	
	@Test
	public void returnChangeTest() {
		v1.addItem(candy,"A");
		v1.insertMoney(2.00);
		
		v1.returnChange();
		assertEquals(v1.getBalance(), 0.0, 0.0);
	}
	
	@After
	public void tearDown() throws Exception {
		v1 = null;
	}

	

}
